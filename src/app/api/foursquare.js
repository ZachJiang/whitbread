
angular
.module('app')
.factory('foursquareService', ['$http', function($http) {
  

    var methods = {};
    
    methods.getRecommendedVenues = function(place) {

        return $http({
            url: 'https://api.foursquare.com/v2/venues/search', 
            method: "GET",
            params: {
                client_id: '5CDZB2WQFBGSFLREL2ND5S450LTWFK1XI5CGZMATQUMOW1O0',
                client_secret: 'TIEZUCLHUDFCUZUTN32YJMJRX4L0RK5QMKWMRFUSR15CJLTY',
                near: place,
                v: '20180323',
                limit: 20
            }
         })
        
    }

    methods.getVenueDetail = function(id) {

        return $http({
            url: `https://api.foursquare.com/v2/venues/${id}`, 
            method: "GET",
            params: {
                client_id: '5CDZB2WQFBGSFLREL2ND5S450LTWFK1XI5CGZMATQUMOW1O0',
                client_secret: 'TIEZUCLHUDFCUZUTN32YJMJRX4L0RK5QMKWMRFUSR15CJLTY',
                VENUE_ID: id,
                v: '20180323'
            }
         })
    }

    methods.getPopularVenues = function(place) {

        return $http({
            url: 'https://api.foursquare.com/v2/venues/trending', 
            method: "GET",
            params: {
                client_id: '5CDZB2WQFBGSFLREL2ND5S450LTWFK1XI5CGZMATQUMOW1O0',
                client_secret: 'TIEZUCLHUDFCUZUTN32YJMJRX4L0RK5QMKWMRFUSR15CJLTY',
                near: place,
                v: '20180323',
                limit: 20
            }
         })
    }
    
    return methods;

}]);

