
angular
.module('app')
.controller('HomeController', ['$scope', 'foursquareService', 
function($scope, foursquareService) {
    
    $scope.place = '';
    
    $scope.searchVenues = function() {
        $scope.recommendedVenues = [];

        foursquareService.getRecommendedVenues($scope.place)
        .then(function(response) {
            if (response.data.response.venues)
            {
                let venues =  response.data.response.venues;
                for (let venue of venues) {
                   
                    foursquareService.getVenueDetail(venue.id)
                    .then(function(response) {
                        let result = response.data.response.venue;
                        let photo = '';
                        if (result.photos.count)
                        {
                            let item  = result.photos.groups[0].items[0]
                            photo = item.prefix + '500x500' + item.suffix;
                    
                        }
                        $scope.recommendedVenues.push({
                            id: result.id,
                            name: result.name,
                            address: result.location.address || '',
                            phone: result.contact.phone || '',
                            website: result.url || '',
                            photo: photo || '../../images/whitbread-500x500.jpg'
                        });
    
                    }, function(error) {
                        console.log('Something went wrong');
                    })
                   
    
                }

            }
            console.log($scope.recommendedVenues);
        
        }, function(error) {
            console.log('Something went wrong');
        });
        
    }

}]);
